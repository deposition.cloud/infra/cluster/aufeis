import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"
import * as cert from "@pulumi/depo-cert-manager"
import * as fs from "fs"
import dedent from "ts-dedent"

const http = 80
const https = 443

interface Features {
  certManager: boolean,
  ingress: boolean,
  linkerd: boolean,
  oauth2: boolean,
  health: boolean,
  postgres: boolean,
  identity: boolean
}

let config = new pulumi.Config()

const stack = pulumi.getStack()
const tier = pulumi.getProject()

const features = config.requireObject<Features>('features')

let domain = config.require("domain")
console.log(`Booting up ${domain}...`)

/*
* ingress controller
*/

const ingressShortName = 'in'
const ingressPrefix = 'ingress'
const ingressNamespace = new k8s.core.v1.Namespace(ingressPrefix, {
  metadata: { labels: { name: ingressPrefix, stack, tier } }
})

const ingress = new k8s.helm.v3.Chart(ingressShortName, {
  repo: "ingress-nginx",
  chart: "ingress-nginx",
  namespace: ingressNamespace.metadata.name
}, {
  parent: ingressNamespace
})

/*
* cert manager
*/

const certmanagerShortName = 'vouch'
const certmanagerPrefix = 'cert-manager'
const certmanagerNamespace = new k8s.core.v1.Namespace(certmanagerPrefix, {
  metadata: { labels: { name: certmanagerShortName, stack, tier } }
}, {
  parent: ingressNamespace
})

const certmanager = new k8s.helm.v3.Chart(certmanagerShortName, {
  repo: "jetstack",
  chart: "cert-manager",
  values: {
    installCRDs: true,
    podLabels: { stack, tier }
  },
  namespace: certmanagerNamespace.metadata.name,
}, {
  parent: certmanagerNamespace
})

const clusterIssuerName = 'letsencrypt-staging'

const clusterIssuer = new cert.certmanager.v1.ClusterIssuer(
  clusterIssuerName, {
    metadata: {
      labels: { name: certmanagerShortName, stack, tier },
      namespace: certmanagerNamespace.metadata.name
    },
  spec: {
    acme: {
      server: "https://acme-staging-v02.api.letsencrypt.org/directory",
      email: `reply@${domain}`,
      privateKeySecretRef: {
        name: clusterIssuerName
      },
      solvers: [{
        http01: {
          ingress: { class: "nginx" }}
      }]
    }
  }
}, {
  parent: certmanager
})

/*
* postgres and pgadmin
*/

const databaseName = 'keycloak'
const postgresUsername = 'keycloak'

const postgresShortName = 'pg'
const postgresPrefix = 'postgres'
const postgresNamespace = new k8s.core.v1.Namespace(postgresPrefix, {
  metadata: { labels: { name: postgresPrefix, stack, tier } }
}, {
  parent: certmanager
})

const postgresSecretNamePrefix = 'postgres'
const postgresSecret = new k8s.core.v1.Secret(postgresSecretNamePrefix, {
  stringData: {
    'postgresql-password': config.requireSecret("postgresPassword"),
    'postgresql-postgres-password': config.requireSecret("postgresRootPassword"),
    'postgresql-replication-password': config.requireSecret("postgresReplicationPassword")
  },
  metadata: { namespace: postgresNamespace.metadata.name }
}, {
  parent: postgresNamespace
})

const postgres = new k8s.helm.v3.Chart(postgresShortName, {
  repo: "bitnami",
  chart: "postgresql",
  values: {
    postgresqlUsername: postgresUsername,
    replication: { enabled: true },
    volumePermissions: { enabled: true },
    existingSecret: postgresSecret.metadata.name
  },
  namespace: postgresNamespace.metadata.name
}, {
  dependsOn: [ certmanager, ingress ]
})

const postgresHost = pulumi.interpolate`pg-postgresql.${postgresNamespace.metadata.name}.svc.cluster.local`

const pgadminShortName = 'pga'

const pgadmin = new k8s.helm.v3.Chart(pgadminShortName, {
  repo: "runix",
  chart: "pgadmin4",
  values: {
    env: {
      email: config.requireSecret("adminEmail"),
      password: config.requireSecret("pgadminPassword"),
    },
    VolumePermissions: { enabled: true }
  },
  namespace: postgresNamespace.metadata.name
}, {})

const pgadminHost = `postgres.${domain}`

const pgadminIngress = new k8s.networking.v1beta1.Ingress(pgadminShortName, {
  metadata: {
    namespace: postgresNamespace.metadata.name,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [pgadminHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: pgadminHost,
      http: {
        paths: [{
          backend: { serviceName: `${pgadminShortName}-pgadmin4`, servicePort: 80 },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  dependsOn: [pgadmin]
})

/*
* keycloak identity
*/
const authHost = `auth.${domain}`

if (features.identity) {

  const identityShortName = 'id'
  const identityPrefix = 'identity'
  const identityNamespace = new k8s.core.v1.Namespace(identityPrefix, {
    metadata: { labels: { name: identityPrefix, stack, tier } }
  }, { parent: postgres })

  const postgresKeycloakSecretNamePrefix = 'pgsecret'
  const postgresKeycloakSecret = new k8s.core.v1.Secret(postgresKeycloakSecretNamePrefix, {
    metadata: {
      namespace: identityNamespace.metadata.name
    },
    stringData: {
      KEYCLOAK_DATABASE_PASSWORD: config.requireSecret("postgresPassword"),
      KEYCLOAK_DATABASE_HOST: postgresHost,
      KEYCLOAK_DATABASE_NAME: 'keycloak',
      KEYCLOAK_DATABASE_USER: 'keycloak',
    }
  }, {
    parent: identityNamespace
  })

  const keycloakAuthSecretNamePrefix = 'auth'
  const keycloakAuthSecret = new k8s.core.v1.Secret(keycloakAuthSecretNamePrefix, {
    metadata: {
      namespace: identityNamespace.metadata.name
    },
    stringData: {
      'postgresql-password': config.requireSecret("postgresPassword"),
      'admin-password': config.requireSecret("keycloakAdminPassword"),
      'management-password': config.requireSecret("keycloakManagementPassword"),
    }
  }, {
    parent: identityNamespace
  })

  const identity = new k8s.helm.v3.Chart(identityShortName, {
    repo: "bitnami",
    chart: "keycloak",
    values: {
      service: { type: 'ClusterIP' },
      postgresql: { enabled: false },
      externalDatabase: {
        existingSecret: postgresKeycloakSecret.metadata.name
      },
      auth: {
        adminUser: 'admin',
        managementUser: 'manager',
        existingSecret: keycloakAuthSecret.metadata.name
      },
      metrics: { enabled: false }
    },
    namespace: identityNamespace.metadata.name
  }, {
    parent: postgres,
    dependsOn: [ ingress, certmanager ]
  })

  const identityIngress = new k8s.networking.v1beta1.Ingress(identityPrefix, {
    metadata: {
      namespace: identityNamespace.metadata.name,
      annotations: {
        'kubernetes.io/ingress.class': 'nginx',
        'nginx.ingress.kubernetes.io/backend-protocol': 'https',
        'nginx.ingress.kubernetes.io/ssl-passthrough': 'true',
        // 'nginx.ingress.kubernetes.io/ssl-redirect': 'false',

        // Add the following line (staging first for testing, then apply the prod issuer)
        'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
      }
    },
    spec: {
      tls: [{
        hosts: [authHost],
        secretName: 'default-tls-secret'
      }],
      rules: [{
        host: authHost,
        http: {
          paths: [{
            backend: { serviceName: 'id-keycloak', servicePort: https },
            path: '/',
            pathType: 'Prefix'
          }]
        }
      }]
    }
  }, {
    parent: identity
  })
}

/*
* health check deployment
*/

const healthPrefix = 'health'
const healthNamespace = new k8s.core.v1.Namespace(healthPrefix, {
  metadata: { labels: { name: healthPrefix, stack, tier } }
}, {
  parent: ingress
})
const health = healthNamespace.metadata.name

const healthAppName = 'health'
const healthAppLabels = { app: healthAppName, stack, tier }

const healthDeployment = new k8s.apps.v1.Deployment(
  healthAppName, {
  metadata: {
    namespace: health,
    labels: healthAppLabels,
  },
  spec: {
    selector: { matchLabels: healthAppLabels },
    replicas: 1,
    template: {
      metadata: { labels: healthAppLabels },
      spec: { containers: [{ name: healthAppName, image: 'nginx' }] }
    }
  }
}, {
  parent: healthNamespace
})

const healthService = new k8s.core.v1.Service(healthAppName, {
  metadata: {
    namespace: health,
    labels: healthDeployment.spec.template.metadata.labels
  },
  spec: {
    type: "ClusterIP",
    ports: [{ port: http, targetPort: http, protocol: "TCP" }],
    selector: healthAppLabels
  }
}, {
  parent: healthNamespace
})

const healthHost = `${healthAppName}.${domain}`

const healthIngress = new k8s.networking.v1beta1.Ingress(healthAppName, {
  metadata: {
    namespace: health,
    labels: healthDeployment.spec.template.metadata.labels,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [healthHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: healthHost,
      http: {
        paths: [{
          backend: { serviceName: healthService.metadata.name, servicePort: http },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }
    ]
  }
}, {
  parent: healthService
})

/*
* linkerd
*/

const linkerdShortName = 'mesh'
const linkerdPrefix = 'linkerd'
const linkerdNamespace = new k8s.core.v1.Namespace(linkerdPrefix, {
  metadata: { labels: { name: linkerdPrefix, stack, tier } }
}, {
  parent: ingressNamespace
})

let aYearFromNow = new Date()
aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1)

const identityTrustAnchorsPEM: string = fs.readFileSync('ca/ca.crt', { encoding: 'utf8', flag: 'r' }).toString().trim()
const crtPEM: string = fs.readFileSync('ca/issuer.crt', { encoding: 'utf8', flag: 'r' }).toString().trim()
const keyPEM: string = fs.readFileSync('ca/issuer.key', { encoding: 'utf8', flag: 'r' }).toString().trim()

const linkerd = new k8s.helm.v3.Chart(linkerdShortName, {
  repo: "linkerd",
  chart: "linkerd2",
  values: {
    installNamespace: false,
    namespace: certmanagerNamespace.metadata.name,
    podLabels: { stack, tier },
    identityTrustAnchorsPEM,
    identity: {
      issuer: {
        tls: { crtPEM, keyPEM },
        crtExpiry: aYearFromNow.toISOString()
      }
    }
  },
  namespace: linkerdNamespace.metadata.name
}, {
  //parent: identity,
  dependsOn: [certmanager, ingress]
})

/*
* dashboard (ingress + role)
*/

const dashBasicShortName = 'dash-basic'
const kubeSystemNamespaceName = 'kube-system'
const kubernetesDashboardServiceName = 'kubernetes-dashboard'

const dashBasicHost = `${dashBasicShortName}.${domain}`

const dashboardServiceAccount = new k8s.core.v1.ServiceAccount('admin-user', {
  metadata: { namespace: kubeSystemNamespaceName, labels: { stack, tier } }
})

const dashboardClusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding('admin-user', {
  metadata: { labels: { stack, tier } },
  roleRef: {
    apiGroup: 'rbac.authorization.k8s.io',
    kind: 'ClusterRole',
    name: 'cluster-admin'
  },
  subjects: [{
    kind: 'ServiceAccount',
    name: dashboardServiceAccount.metadata.name,
    namespace: dashboardServiceAccount.metadata.namespace
  }]
})

const dashBasicIngress = new k8s.networking.v1beta1.Ingress(dashBasicShortName, {
  metadata: {
    namespace: kubeSystemNamespaceName,
    annotations: {
      'nginx.ingress.kubernetes.io/backend-protocol': 'https',
      'nginx.ingress.kubernetes.io/ssl-passthrough': 'true',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    },
    labels: { stack, tier }
  },
  spec: {
    tls: [{
      hosts: [dashBasicHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: dashBasicHost,
      http: {
        paths: [{
          backend: { serviceName: kubernetesDashboardServiceName, servicePort: https },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  parent: certmanager,
  dependsOn: [ ingress ]
})

const dashOAuth2ShortName = 'dash-oauth2'
const dashOAuth2Host = `${dashOAuth2ShortName}.${domain}`

const oauth2ProxyShortName = 'oauth2'
const oauth2ProxyPrefix = 'oauth2-proxy'
const oauth2ProxyNamespace = new k8s.core.v1.Namespace(oauth2ProxyPrefix, {
  metadata: { labels: { name: oauth2ProxyPrefix, stack, tier } }
}, {
  parent: ingressNamespace
})

const oauth2githubSecretNamePrefix = 'oauth2github'
const oauth2githubSecret = new k8s.core.v1.Secret(oauth2githubSecretNamePrefix, {
  stringData: {
    'client-id': config.requireSecret("oauth2githubClientID"),
    'client-secret': config.requireSecret("oauth2githubClientSecret"),
    'cookie-secret': config.requireSecret("oauth2githubCookieSecret")
  },
  metadata: { namespace: oauth2ProxyNamespace.metadata.name }
}, {
  parent: certmanager,
  dependsOn: [ ingress ]
})

const oauth2Proxy = new k8s.helm.v3.Chart(oauth2ProxyShortName, {
  repo: "oauth2-proxy",
  chart: "oauth2-proxy",
  values: {
    config: {
      configFile: dedent`provider='github'
email_domains='*'
whitelist_domains=['.${domain}']
cookie_domains=['.${domain}']`,
      existingSecret: oauth2githubSecret.metadata.name
    }
  },
  namespace: oauth2ProxyNamespace.metadata.name
}, {
  dependsOn: [ certmanager, ingress ]
})

const oauth2Host = `${oauth2ProxyShortName}.${domain}`

const oauth2ProxyIngress = new k8s.networking.v1beta1.Ingress(oauth2ProxyShortName, {
  metadata: {
    namespace: oauth2ProxyNamespace.metadata.name,
    annotations: {
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    },
    labels: { stack, tier }
  },
  spec: {
    tls: [{
      hosts: [oauth2Host],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: oauth2Host,
      http: {
        paths: [{
          backend: { serviceName: `${oauth2ProxyShortName}-${oauth2ProxyPrefix}`, servicePort: http },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  parent: certmanager,
  dependsOn: [ ingress ]
})

const healthOAuth2Host = `${healthAppName}-${oauth2ProxyShortName}.${domain}`

const healthOAuth2Ingress = new k8s.networking.v1beta1.Ingress(`${healthAppName}-${oauth2ProxyShortName}`, {
  metadata: {
    namespace: health,
    labels: healthDeployment.spec.template.metadata.labels,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      'nginx.ingress.kubernetes.io/auth-url': pulumi.interpolate `http://${oauth2ProxyShortName}-${oauth2ProxyPrefix}.${oauth2ProxyNamespace.metadata.name}.svc.cluster.local/oauth2/auth`,
      'nginx.ingress.kubernetes.io/auth-signin': `https://${oauth2ProxyShortName}.${domain}/oauth2/start`,
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [healthOAuth2Host],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: healthOAuth2Host,
      http: {
        paths: [{
          backend: { serviceName: healthService.metadata.name, servicePort: http },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }
    ]
  }
}, {
  parent: healthService
})

// Follow instructions: https://kubernetes.github.io/ingress-nginx/examples/auth/oauth-external-auth/
const dashOAuth2Ingress = new k8s.networking.v1beta1.Ingress(dashOAuth2ShortName, {
  metadata: {
    namespace: kubeSystemNamespaceName,
    annotations: {
      'nginx.ingress.kubernetes.io/backend-protocol': 'https',
      //'nginx.ingress.kubernetes.io/ssl-passthrough': 'true',
      'nginx.ingress.kubernetes.io/auth-url': pulumi.interpolate `http://${oauth2ProxyShortName}-${oauth2ProxyPrefix}.${oauth2ProxyNamespace.metadata.name}.svc.cluster.local/oauth2/auth`,
      'nginx.ingress.kubernetes.io/auth-signin': `https://${oauth2ProxyShortName}.${domain}/oauth2/start`,
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    },
    labels: { stack, tier }
  },
  spec: {
    tls: [{
      hosts: [dashOAuth2Host],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: dashOAuth2Host,
      http: {
        paths: [{
          backend: { serviceName: kubernetesDashboardServiceName, servicePort: https },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  parent: oauth2Proxy,
  dependsOn: [ certmanager, ingress ]
})

/*
* kibana (ingress) for fluentd add-on
*/

const kibanaShortName = 'kibana'
const kibanaServiceName = 'kibana-logging'
const kibanaServicePort = 5601

const kibanaHost = `${kibanaShortName}.${domain}`

const kibanaIngress = new k8s.networking.v1beta1.Ingress(kibanaShortName, {
  metadata: {
    namespace: kubeSystemNamespaceName,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    },
    labels: { stack, tier }
  },
  spec: {
    tls: [{
      hosts: [kibanaHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: kibanaHost,
      http: {
        paths: [{
          backend: { serviceName: kibanaServiceName, servicePort: kibanaServicePort },
          path: '/',
          pathType: 'Prefix'
        }]
      }
    }]
  }
}, {
  //parent: identity,
  dependsOn: [ ingress, certmanager ]
})

export const ip = healthService.spec.clusterIP
export const authURL = features.identity ? `https://${authHost}` : undefined
export const healthURL = `https://${healthHost}`
export const healthOAuth2URL = `https://${healthOAuth2Host}`
export const dashBasicURL = `https://${dashBasicHost}`
export const dashOAuth2URL = `https://${dashOAuth2Host}`
export const oauth2URL = `https://${oauth2Host}`
export const getDashBasicTokenBash = pulumi.interpolate`kubectl get secret/${dashboardServiceAccount.secrets[0].name} -n kube-system -o go-template='{{.data.token | base64decode}}'`
export const kibanaURL = `https://${kibanaHost}`
export const postgresqlHost = postgresHost
