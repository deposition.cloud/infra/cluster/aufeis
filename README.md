# Aufeis

> a sheet-like mass of layered ice that forms from successive flows of ground water during freezing temperatures

Base layer of cluster services for an operational personal cloud.

- nginx ingress controller
- cert-manager
- nginx deployment to test, i.e.: [nginx.deposition.cloud](https://nginx.deposition.cloud)

## Prerequisites

Requires a working MicroK8s cluster behind an Internet-connected router, and a domain name.

## Design

2 clusters on the same LAN behind an internet facing router.

The `live` cluster is the production personal cloud, a 3-node HA bare metal setup.

The `omega` cluster is a few commits ahead, and resides on a 1-node VM, and also acts as a fail-over backup cluster for when `live` goes down.

## Develop and Deploy

``` bash
pulumi config set kubernetes:context omega # or
pulumi config set kubernetes:context live
```

Pre-requisites for Helm.

``` bash
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
```

### Pulumi Stack Configurations

``` bash
pulumi config set --plaintext tld deposition.cloud # replace with your domain
```

### NGINX Ingress Controller

``` bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

### Cert Manager

``` bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
```

``` bash
cd cert-manager/
mkdir crds
wget https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.crds.yaml --output-document crds/cert-manager.crds.yaml
crd2pulumi crds/cert-manager.crds.yaml --nodejsName depo-cert-manager --nodejsPath cert-manager-types --force
npm install typescript --save-dev
cd ../
npm install --save cert-manager/cert-manager-types/
```

### OAuth2

``` bash
helm repo add oauth2-proxy https://oauth2-proxy.github.io/manifests
helm repo update
```

### Keycloak identity provider

``` bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add runix https://helm.runix.net
helm repo update
```

### Deploy

``` bash
pulumi up -f -y
```

We should now be able to securely access: [nginx.deposition.cloud](https://nginx.deposition.cloud)

Yey! :fireworks:

## Troubleshooting

Many things could go wrong. :smile:
